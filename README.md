<!---
Full module name and description.
-->
# farmOS FarmLab
farmOS integration with [FarmLab](https://www.farmlab.com.au/)

This module is an add-on for the [farmOS](http://drupal.org/project/farm)
distribution.

<!---
Geting started.
-->
## Getting started

<!---
Document installation steps.
-->
### Installation

Install as you would normally install a contributed drupal module.

```
composer require 'drupal/farm_farmlab:^1.0'
```

### Configuration

## Authentication

A FarmLab API key is required to use this module. Contact FarmLab for more info.

## Features

### Boundaries

This module adds the ability to sync farm boundaries between farmOS and FarmLab.

### Cadastrals

This module integrates with the FarmLab Cadastrals API to facilitate easily
syncing cadastrals info farmOS Land Assets.

## Maintainers

Current maintainers:
- Paul Weidner [@paul121](https://github.com/paul121)

## Sponsors
This project has been sponsored by:
- [Regen Digital](https://regenfarmersmutual.com/regendigital/)
